# Coding Challenge: Digital Assistant Service- Release Document

### Introduction

The digital assistant service is built using SpringBoot 3.2.5 and PostgreSQL database and can be run locally or on could with following configurations.

### Prerequisites
1. Java Development Kit (JDK): Ensure JDK 17 or later installed on your system.
2. Gradle 2.x: Since Spring 3.2.5 doesn't come with Spring Boot's Gradle wrapper, its required to install Gradle 2.x on the system if not present already.
3. Running postgreSQL Database server either locally or remotely.

### Instructions to Run the Service
1. Clone the Repository: Clone theservice joule from the link below https://gitlab.com/aakarshprojects/DigitalAssistantService or download the provided Spring Boot project from the attached joule.zip in the email.

2. Upgrade build.gradle if specific spring versions need to be used otherwise keep it same.

3. Update src/main/resources/application.properties for database connection: 

```
spring.datasource.url=jdbc:postgresql://localhost:5432/postgres
### Replace with your desired database URL, name and password
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.username=postgres
spring.datasource.password=******** 
```

4. Start the application using command   gradle bootRun

5. Access Swagger UI: Open a web browser and navigate to :
http://localhost:8080/swagger-ui.html

6. Test the Endpoints: Use Swagger UI to test the API endpoints.

### Rest APIs Description

#### Service 1-    
User can define a name and text response string for a digital assistant.      
> EndPoint:   localhost:8080/chatAssistant/create
> Http Method :  POST,  Body:   JSON   Output:   JSON
> Payload:
```
{"username":"aakarsh","assistantname":"Google","response":"Hi,Google is here to help you?"}
```
> Functionality:
1. A user can add unique assistant names and their response strings. 
2. Once the Api is called with a valid payload it saves this information in DB.

#### Service 2-    
User sends a text message to the named assistant and receives the defined string.      
> EndPoint:   localhost:8080/chatAssistant/message
> Http Method :  POST,  Body:   JSON    
> Payload:
```
{"username":"aakarsh","assistantname":"Google"}
```
> Functionality:
1. A user can add multiple unique assistant names and their response strings.
2. Once the Api is called with a valid payload it saves this information in DB.
> Output:   String
Hi,Google is here to help you?


### Notes
1. APIs can be accessed through any REST client Postman etc:
2. Login functionality was not implemented due to time constraint so username has to be specified manually
