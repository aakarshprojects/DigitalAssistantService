package com.assistant.joule.repo;
import java.util.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.Param;

import com.assistant.joule.model.ChatAssistant;

@RepositoryDefinition(domainClass = ChatAssistant.class, idClass = Integer.class)
public interface ChatAssistantRepo extends JpaRepository<ChatAssistant, Integer> {

    @Query("SELECT e FROM ChatAssistant e WHERE e.username = :username AND e.assistantname = :assistantname")
    Optional<ChatAssistant> findByUsernameAndAssistantname(@Param("username") String username, @Param("assistantname") String assistantname);

    Optional<ChatAssistant> findByAssistantname(String assistantname);
}
