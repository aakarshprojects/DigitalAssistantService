package com.assistant.joule.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotBlank;



@Entity
@Table(name = "chat_assistant", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"username", "assistantname"})
})
public class ChatAssistant {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int assistantId;
  @NotBlank
  private String username;
  @NotBlank
  @Column(name = "assistantname")
  private String assistantname;
  @NotBlank
  private String response;  

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }
public String getAssistantname() {
    return assistantname;
}

public void setAssistantname(String assistantname) {
    this.assistantname = assistantname;
}

public String getResponse() {
    return response;
}

public void setResponse(String response) {
    this.response = response;
}
  
}
