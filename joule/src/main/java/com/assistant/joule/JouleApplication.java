package com.assistant.joule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class JouleApplication {

	public static void main(String[] args) {
		SpringApplication.run(JouleApplication.class, args);
	}

}
