package com.assistant.joule.controller;

import org.springframework.web.bind.annotation.RestController;

import com.assistant.joule.model.ChatAssistant;
import com.assistant.joule.service.ChatAssistantService;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;




@RestController
@RequestMapping("/chatAssistant")
public class ChatAssistantController {
    @Autowired
    private ChatAssistantService chatAssistantService;
   
    @PostMapping("/create")
    public ResponseEntity<ChatAssistant> createAssistant(@Validated @RequestBody ChatAssistant chatAssistant) {
        return chatAssistantService.saveChatAssistant(chatAssistant);
    }

    /**
     * @param assistant
     * @return
     */
    @PostMapping("/message")
    public ResponseEntity<String> sendMessage(@RequestBody ChatAssistant assistant) {
        return chatAssistantService.sendMessage(assistant);
}
}
    

