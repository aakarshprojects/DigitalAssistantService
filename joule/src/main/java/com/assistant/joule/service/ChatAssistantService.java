package com.assistant.joule.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.assistant.joule.model.ChatAssistant;
import com.assistant.joule.repo.ChatAssistantRepo;

import java.util.Optional;

@Service
public class ChatAssistantService {

    @Autowired
    private ChatAssistantRepo assistantRepository;

    public ResponseEntity<ChatAssistant> saveChatAssistant(ChatAssistant assistant) {
         Optional<ChatAssistant> assistantOptional=assistantRepository.findByUsernameAndAssistantname(assistant.getUsername(),
        assistant.getAssistantname());
         if (assistantOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build(); // Handle username conflict
        }
        ChatAssistant savedAssistant = assistantRepository.save(assistant);
        return ResponseEntity.ok(savedAssistant);
    }

   public ResponseEntity<String> sendMessage(ChatAssistant assistant) {
        Optional<ChatAssistant> assistantOptional = 
        assistantRepository.findByUsernameAndAssistantname(assistant.getUsername(),  assistant.getAssistantname());
        if (!assistantOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(assistantOptional.get().getResponse());
    }
}
